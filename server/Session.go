package server

import (
	"bufio"
	"fmt"
	"os"
	"regexp"

	"github.com/librespot-org/librespot-golang/librespot/core"
	"github.com/nanu-c/qml-go"
	"github.com/zmb3/spotify/v2"
)

type Settings struct {
	Quality   int //0 == low, 1 == medium, 2 == high
	Theme     int //0 == dark, 1 == light, 2 == system
	ErrorSong int // 0 == silence, 1 == error
}

type Contributors struct {
	Name      string
	AvatarUrl string
	Link      string
}

type Session struct {
	Root          qml.Object
	spotSession   *core.Session
	spotAPIClient *spotify.Client
	IsLogged      bool
	Username      string
	Settings      *Settings
	playlists     map[string]*Playlists
	playlist      map[string]*Playlist
	albums        map[string]*Album
	shows         map[string]*Shows
	show          map[string]*Show
	searchResults *SearchResults
	tracks        map[string]*Track
	contributors  []*Contributors
	port          int
}

func NewSession(port int) *Session {
	spotSession := &Session{
		IsLogged: false,
		Username: "",
		Settings: &Settings{
			Quality: 1,
			Theme:   0,
		},
		albums:       make(map[string]*Album),
		playlists:    make(map[string]*Playlists),
		playlist:     make(map[string]*Playlist),
		tracks:       make(map[string]*Track),
		shows:        make(map[string]*Shows),
		show:         make(map[string]*Show),
		contributors: loadContributors(),
		port:         port,
	}
	spotSession.albums["queue"] = &Album{Size: 0, Name: "Queue", Uuid: "queue", tracks: make([]*Track, 0)}
	return spotSession
}

func loadContributors() []*Contributors {
	contributors := make([]*Contributors, 0)

	file, err := os.Open("CONTRIBUTORS.md")
	if err != nil {
		fmt.Println(err)
		return contributors
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var txt string
	r := regexp.MustCompile("^\\|\\s*!\\[[^\\]]+\\]\\(([^\\]]+)\\)\\s*\\|\\s*([^\\s]+)\\s*\\|\\s*\\[[^\\]]+\\]\\(([^\\)]+)")
	var res []string
	for scanner.Scan() {
		txt = scanner.Text()
		res = r.FindStringSubmatch(txt)
		if len(res) > 1 {
			contributors = append(contributors, &Contributors{Name: res[2], AvatarUrl: res[1], Link: res[3]})
		}
	}
	return contributors
}
