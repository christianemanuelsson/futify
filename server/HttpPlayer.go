package server

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/librespot-org/librespot-golang/Spotify"
)

type HttpPlayer struct {
	session       *Session
	trackInPlayer map[string]io.ReadSeeker
}

type TrackFactory interface {
	GetTrack(string) (*Track, bool)
	GetSettings() *Settings
}

func NewHttpPlayer(session *Session) *HttpPlayer {
	return &HttpPlayer{session: session, trackInPlayer: make(map[string]io.ReadSeeker)}
}

func (p *HttpPlayer) Start() {
	http.HandleFunc("/song", p.handleHttpSong)
	go func() {
		server := http.Server{
			Addr: fmt.Sprintf(":%d", p.session.port),
		}
		log.Fatal(server.ListenAndServe())
	}()
}

func (p *HttpPlayer) handleHttpSong(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Request receive", r.Method, r.URL, r.Header)

	id, ok := r.URL.Query()["id"]
	if !ok {
		fmt.Println("Url without id query")
		p.sendError(w, r, "null")
		return
	}

	if audioFile, ok := p.trackInPlayer[id[0]]; ok {
		p.sendFromCache(w, r, id[0], audioFile)
		return
	}

	t, ok := p.session.tracks[id[0]]
	if !ok {
		fmt.Println("Tracks id not in cache")
		p.sendError(w, r, id[0])
		return
	}

	// As a demo, select the OGG 160kbps variant of the track. The "high quality" setting in the official Spotify
	// app is the OGG 320kbps variant.
	var selectedFile *Spotify.AudioFile
	var gid []byte
	if !t.isEpisode {

		track, err := p.session.spotSession.Mercury().GetTrack(t.Id)
		if err != nil {
			fmt.Println("Error loading track: ", err)
			p.sendError(w, r, id[0])
			return
		}

		fmt.Println("Download Track:", t.Id, "With name:", track.GetName())

		for _, file := range track.GetFile() {
			if selectedFile == nil && file.FileId != nil {
				selectedFile = file
			}
			if file.GetFormat() == Spotify.AudioFile_OGG_VORBIS_96 && p.session.Settings.Quality < 1 && file.FileId != nil {
				selectedFile = file
				break
			} else if file.GetFormat() == Spotify.AudioFile_OGG_VORBIS_160 && p.session.Settings.Quality == 1 && file.FileId != nil {
				selectedFile = file
				break
			} else if file.GetFormat() == Spotify.AudioFile_OGG_VORBIS_320 && p.session.Settings.Quality > 1 && file.FileId != nil {
				selectedFile = file
				break
			}
		}

		if selectedFile == nil || selectedFile.FileId == nil {
			fmt.Println("Track error no valid file found!?")
			return
		}

		gid = track.GetGid()
	} else {
		episode, selectedEpisode := p.tryEpisode(w, r, id, t)

		if selectedEpisode == nil || selectedEpisode.FileId == nil {
			fmt.Println("Track error no valid file found!?")
			p.sendError(w, r, id[0])
			return
		}

		selectedFile = selectedEpisode
		gid = episode.GetGid()
	}

	fmt.Println(fmt.Sprintf("SelectedFile to download %s", selectedFile.GetFormat().String()))

	audioFile, err := p.session.spotSession.Player().LoadTrack(selectedFile, gid)
	if err != nil {
		fmt.Println("audioFile error", err)
		p.sendError(w, r, id[0])
		return

	}
	p.sendTrack(w, r, id[0], NewFutifyAudioFile(t, audioFile))
}

func (p *HttpPlayer) tryEpisode(w http.ResponseWriter, r *http.Request, id []string, t *Track) (*Spotify.Episode, *Spotify.AudioFile) {
	track, err := p.session.spotSession.Mercury().GetEpisode(t.Id)
	if err != nil {
		fmt.Println("Error loading track: ", err)
		p.sendError(w, r, id[0])
		return nil, nil
	}

	fmt.Println("Download Track:", t.Id, "With name:", track.GetName())

	// As a demo, select the OGG 160kbps variant of the track. The "high quality" setting in the official Spotify
	// app is the OGG 320kbps variant.
	var selectedFile *Spotify.AudioFile
	for _, file := range track.GetFile() {
		println(file.GetFormat().String())
		if selectedFile == nil && file.FileId != nil {
			selectedFile = file
		}
		if file.GetFormat() == Spotify.AudioFile_OGG_VORBIS_96 && p.session.Settings.Quality < 1 && file.FileId != nil {
			selectedFile = file
			break
		} else if file.GetFormat() == Spotify.AudioFile_OGG_VORBIS_160 && p.session.Settings.Quality == 1 && file.FileId != nil {
			selectedFile = file
			break
		} else if file.GetFormat() == Spotify.AudioFile_OGG_VORBIS_320 && p.session.Settings.Quality > 1 && file.FileId != nil {
			selectedFile = file
			break
		}
	}

	if selectedFile == nil || selectedFile.FileId == nil {
		fmt.Println("Track error no valid file found!?")
		p.sendError(w, r, id[0])
		return nil, nil
	}
	return track, selectedFile
}

func (p *HttpPlayer) sendError(w http.ResponseWriter, r *http.Request, idTrack string) {
	song := "silence.ogg"
	if p.session.Settings.ErrorSong == 1 {
		song = "error.ogg"
	}
	f, err := http.Dir("assets/").Open(song)
	if err != nil {
		fmt.Println("Can't find error.ogg?")
		return
	}
	p.trackInPlayer[idTrack] = f
	fmt.Println("Tracks id not in cache")
	http.ServeContent(w, r, idTrack, time.Time{}, p.trackInPlayer[idTrack])
}

func (p *HttpPlayer) sendTrack(w http.ResponseWriter, r *http.Request, idTrack string, audioFile *FutifyAudioFile) {
	p.trackInPlayer[idTrack] = audioFile
	http.ServeContent(w, r, idTrack, time.Time{}, p.trackInPlayer[idTrack])
}

func (p *HttpPlayer) sendFromCache(w http.ResponseWriter, r *http.Request, idTrack string, audioFile io.ReadSeeker) {
	http.ServeContent(w, r, idTrack, time.Time{}, audioFile)
}
